// Fill out your copyright notice in the Description page of Project Settings.

#include "YottoEffectController.h"


AYottoEffectController::AYottoEffectController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AYottoEffectController::BeginPlay()
{
	Super::BeginPlay();
	
	FString configFileJson;
	bool isLoaded = UJsonConfigBFL::LoadConfigFile(ConfigFileName, configFileJson);

	if (isLoaded)
	{
		FJsonObjectConverter::JsonObjectStringToUStruct<FConfigData>(configFileJson, &ConfigData, 0, 0);
	}
	else
	{
		GenerateNewConfigFile();
		FJsonObjectConverter::UStructToJsonObjectString(FConfigData::StaticStruct(), &ConfigData, configFileJson, 0, 0, 0);
		UJsonConfigBFL::SaveConfigFile(ConfigFileName, configFileJson);
	}
	
	UPlatformIntegration::SetConfigData(ConfigData);

	if (ConfigData.bUseEffects)
	{
		UE_LOG(LogScript, Warning, TEXT("Trying StartSession"));
		
		UPlatformIntegration::startSession(ConfigData.bIsKidMode);
		
		UE_LOG(LogScript, Warning, TEXT("StartSession"));

		bIsStarted = true;
	}
	else
	{
		UE_LOG(LogScript, Warning, TEXT("Session without effects and movement"));
	}
}

void AYottoEffectController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DriveSimulation();

	for (int i = 0; i < ActiveEffects.Num(); i++)
	{
		ActiveEffects[i].DurationLeft = ActiveEffects[i].DurationLeft - DeltaTime;
		if (ActiveEffects[i].DurationLeft <= 0)
		{
			DisableEffect(i);
			ActiveEffects.RemoveAt(i);
			i--;
		}
	}
}

void AYottoEffectController::DriveSimulation()
{
	if (!ConfigData.bSimulateCoordinates || !bIsStarted)
		return;
		
	float coord = sin(GetWorld()->GetDeltaSeconds());
	UPlatformIntegration::driveSetCoord(0, coord);
	UPlatformIntegration::driveSetCoord(1, coord);
}

bool AYottoEffectController::IsConnected()
{
	return bIsStarted;
}

void AYottoEffectController::ApplyEffect(UObject* WorldContextObject, EYottoEffectType effectType, EYottoEffectPosition effectPosition, float duration, int32 value)
{
	AYottoEffectController* EffectController = nullptr;

	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject);
	if (World == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("WorldContextObject not valid! Effect failed."));
		return;
	}

	for (TActorIterator<AYottoEffectController> ActorItr(World); ActorItr; ++ActorItr)
	{
		EffectController = *ActorItr;
		break;
	}

	if (EffectController != nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("Effect Controller was found!"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Effect Controller not found! Try to Spawn..."));
		
		FActorSpawnParameters SpawnParameters;
		FVector Location(0.0f, 0.0f, 0.0f);
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		EffectController = World->SpawnActor<AYottoEffectController>(Location, Rotation, SpawnParameters);
		UE_LOG(LogTemp, Log, TEXT("Effect Controller was spawned!"));
	}

	EffectController->ApplyEffectInternal(effectType, effectPosition, duration, value);
}

void AYottoEffectController::PrintLog(FString message, uint8 boardIndex, int32 effectNumber, int32 value, bool isAnalog, bool isDiscrete, bool isSpecial)
{
	FString type;
	if (isAnalog)
		type = "Analog";
	else if (isDiscrete)
		type = "Discrete";
	else if (isSpecial)
		type = "Special";
	else
		type = "NoType";

	UE_LOG(LogScript, Warning, TEXT("%s BoardIndex: %d; EffectNumber: %d; Value: %d; %s"), *message, boardIndex, effectNumber, value, *type);
}

void AYottoEffectController::DisableEffect(int32 index)
{
	if (ActiveEffects[index].bIsAnalog)
	{
		UPlatformIntegration::effectSetAnalog(ActiveEffects[index].BoardIndex, ActiveEffects[index].EffectNumber, 0);
	}
	else if (ActiveEffects[index].bIsSpecial)
	{
		UPlatformIntegration::effectSetSpecial(ActiveEffects[index].BoardIndex, ActiveEffects[index].EffectNumber, 0);
	}
	else if (ActiveEffects[index].bIsDiscrete)
	{
		UPlatformIntegration::effectSetDiscrete(ActiveEffects[index].BoardIndex, ActiveEffects[index].EffectNumber, false);
	}
	else
		return;

	PrintLog("DisableEffect", ActiveEffects[index].BoardIndex, ActiveEffects[index].EffectNumber, 0, ActiveEffects[index].bIsAnalog, ActiveEffects[index].bIsDiscrete, ActiveEffects[index].bIsSpecial);
}

void AYottoEffectController::GenerateNewConfigFile()
{
	FSingleEffect effect;
	FEffectPositionContainer container;

	container.effects.Add(effect);
	container.effects.Add(effect);
	container.effects.Add(effect);
	
	ConfigData.effectBoards.Add(container);
	ConfigData.effectBoards.Add(container);
}

int32 AYottoEffectController::GetActiveIndex(uint8 boardIndex, int32 effectNumber, bool isAnalog, bool isDiscrete, bool isSpecial)
{
	int32 currentIndex = -1;
	for (int i = 0; i < ActiveEffects.Num(); i++)
	{
		if (ActiveEffects[i].BoardIndex == boardIndex &&
			ActiveEffects[i].EffectNumber == effectNumber &&
			ActiveEffects[i].bIsAnalog == isAnalog &&
			ActiveEffects[i].bIsDiscrete == isDiscrete &&
			ActiveEffects[i].bIsSpecial == isSpecial)
		{
			currentIndex = i;
			break;
		}
	}
	return currentIndex;
}

void AYottoEffectController::AddActiveEffect(float duration, uint8 boardIndex, int32 effectNumber, uint32 value, bool isAnalog, bool isDiscrete, bool isSpecial)
{
	auto currentIndex = GetActiveIndex(boardIndex, effectNumber, isAnalog, isDiscrete, isSpecial);
	if (currentIndex >= 0)
	{
		ActiveEffects[currentIndex].DurationLeft = duration;
	}
	else
	{
		FEffectTuple newItem;
		newItem.BoardIndex = boardIndex;
		newItem.EffectNumber = effectNumber;
		newItem.DurationLeft = duration;
		newItem.Value = value;
		newItem.bIsAnalog = isAnalog;
		newItem.bIsSpecial = isSpecial;
		newItem.bIsDiscrete = isDiscrete;
		currentIndex = ActiveEffects.Add(newItem);
	}

	if (isAnalog)
	{
		UPlatformIntegration::effectSetAnalog(boardIndex, effectNumber, value);
	}
	else if (isSpecial)
	{
		UPlatformIntegration::effectSetSpecial(boardIndex, effectNumber, value);
	}
	else if (isDiscrete)
	{
		UPlatformIntegration::effectSetDiscrete(boardIndex, effectNumber, value != 0);
	}
	else
	{
		UE_LOG(LogScript, Warning, TEXT("Effect isn`t analog, discrete or special"));
		return;
	}

	PrintLog("EnableEffect", boardIndex, effectNumber, value, isAnalog, isDiscrete, isSpecial);
}

bool AYottoEffectController::GetEffectStruct(EYottoEffectType effectType, EYottoEffectPosition effectPosition, 
	FEffectPositionContainer &OutPositionContainer, FSingleEffect &OutEffectData)
{
	for (int i = 0; i < ConfigData.effectBoards.Num(); i++)
	{
		auto currentBoard = ConfigData.effectBoards[i];
		if (currentBoard.effectsPosition != effectPosition)
			continue;

		for (int j = 0; j < currentBoard.effects.Num(); j++)
		{
			auto currentEffect = currentBoard.effects[j];
			if (currentEffect.effectType != effectType)
				continue;

			OutPositionContainer = currentBoard;
			OutEffectData = currentEffect;
			return true;
		}
	}
	return false;
}

void AYottoEffectController::ApplyEffectInternal(EYottoEffectType effectType, EYottoEffectPosition effectPosition, float duration, uint32 value)
{
	FEffectPositionContainer positionContainer;
	FSingleEffect effectData;
	if (GetEffectStruct(effectType, effectPosition, positionContainer, effectData))
	{
		AddActiveEffect(duration, positionContainer.boardNumber, effectData.effectNumber, value, effectData.isAnalog, effectData.isDiscrete, effectData.isSpecial);
	}
	else
	{
		FString effectTypeStr = EnumHelper::EnumToString("EYottoEffectType", effectType, "NotFoundEffectTypeName");
		FString effectPosStr = EnumHelper::EnumToString("EYottoEffectPosition", effectPosition, "NotFoundEffectPositionName");
		UE_LOG(LogScript, Warning, TEXT("CannotFindEffectStruct for %s  %s"), *effectTypeStr, *effectPosStr);
	}
}

void AYottoEffectController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (ConfigData.bUseEffects)
	{
		UPlatformIntegration::stopSession();
		UE_LOG(LogScript, Warning, TEXT("StopSession"));
	}
}