// Fill out your copyright notice in the Description page of Project Settings.

#include "JsonConfigBFL.h"

bool UJsonConfigBFL::SaveConfigFile(FString fileName, FString string)
{
	return SaveFile(FPaths::GameConfigDir(), fileName, string);
}

bool UJsonConfigBFL::SaveFile(FString directory, FString fileName, FString string)
{
	FString fullPath = FPaths::Combine(directory, fileName);
	fullPath = FPaths::ConvertRelativePathToFull(fullPath);
	UE_LOG(LogScript, Warning, TEXT("SaveFile: %s"), *fullPath);
	if (!FFileHelper::SaveStringToFile(string, *fullPath))
	{
		UE_LOG(LogScript, Error, TEXT("Error, when generating new config file"));
		return false;
	}
	return true;
}

bool UJsonConfigBFL::LoadConfigFile(FString fileName, FString &result)
{
	return LoadFile(FPaths::GameConfigDir(), fileName, result);
}

bool UJsonConfigBFL::LoadFile(FString directory, FString fileName, FString &result)
{
	FString fullPath = FPaths::Combine(directory, fileName);
	fullPath = FPaths::ConvertRelativePathToFull(fullPath);
	UE_LOG(LogScript, Warning, TEXT("LoadFile: %s"), *fullPath);

	if (!FPlatformFileManager::Get().GetPlatformFile().FileExists(*fullPath))
	{
		UE_LOG(LogScript, Error, TEXT("File not found"));
		return false;
	}

	if (!FFileHelper::LoadFileToString(result, *fullPath))
	{
		result = TEXT("Invalid");
		UE_LOG(LogScript, Error, TEXT("Error, when loading file"));
		return false;
	}
	UE_LOG(LogScript, Warning, TEXT("File Loaded: %s"), *result);

	return true;
}


