// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformIntegration.h"

static void* manager = 0;
static FConfigData config;

void UPlatformIntegration::SetConfigData(const FConfigData& newConfigData)
{
	config = newConfigData;
}

bool UPlatformIntegration::isGame()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::isGame(manager);
}

bool UPlatformIntegration::isConnected()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::isConnected(manager);
}

int UPlatformIntegration::getUpdateDelay()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::getUpdateDelay(manager);
}

void UPlatformIntegration::setUpdateDelay(uint8 delay)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::setUpdateDelay(manager, delay);
}

void UPlatformIntegration::setEnabled(bool enabled)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::setEnabled(manager, enabled);
}

void UPlatformIntegration::setDebug(bool debugEnabled)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::setEnabled(manager, debugEnabled);
}

void UPlatformIntegration::restart()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::restart(manager);
}

void UPlatformIntegration::startSession(bool isKidMode)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::startSession(manager, isKidMode);
}

uint8 UPlatformIntegration::joysticksGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::joysticksGetCount(manager);
}

uint8 UPlatformIntegration::arcsGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::arcsGetCount(manager);
}

uint8 UPlatformIntegration::beltsGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::beltsGetCount(manager);
}

uint8 UPlatformIntegration::gatesGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::gatesGetCount(manager);
}

uint8 UPlatformIntegration::boxesGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::boxesGetCount(manager);
}

uint8 UPlatformIntegration::effectsGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::effectsGetCount(manager);
}

uint8 UPlatformIntegration::supervisorsGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::supervisorsGetCount(manager);
}

uint8 UPlatformIntegration::drivesGetCount()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::drivesGetCount(manager);
}

void UPlatformIntegration::stopSession()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::stopSession(manager);
	YottoEffects::DisposePlatformManager(manager);
	manager = 0;
}

bool UPlatformIntegration::joystickGetButton(uint8 joystickIndex, int buttonIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::joystickGetButton(manager, joystickIndex, buttonIndex);
}

float UPlatformIntegration::joystickGetX(uint8 joystickIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::joystickGetX(manager, joystickIndex);
}

float UPlatformIntegration::joystickGetY(uint8 joystickIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::joystickGetY(manager, joystickIndex);
}

bool UPlatformIntegration::arcIsClosed(uint8 arcIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::arcIsClosed(manager, arcIndex);
}

bool UPlatformIntegration::arcIsPreopened(uint8 arcIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::arcIsPreopened(manager, arcIndex);
}

bool UPlatformIntegration::arcMotionIsBlocked(uint8 arcIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::arcMotionIsBlocked(manager, arcIndex);
}

bool UPlatformIntegration::arcIsBlocked(uint8 arcIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::arcIsBlocked(manager, arcIndex);
}

bool UPlatformIntegration::arcIsBroken(uint8 arcIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::arcIsBroken(manager, arcIndex);
}

void UPlatformIntegration::arcSetBlock(uint8 arcIndex, bool blocked)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::arcSetBlock(manager, arcIndex, blocked);
}

bool UPlatformIntegration::beltIsClosed(uint8 beltIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::beltIsClosed(manager, beltIndex);
}

bool UPlatformIntegration::gateIsClosed(uint8 gateIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::gateIsClosed(manager, gateIndex);
}

bool UPlatformIntegration::gateIsBlocked(uint8 gateIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::gateIsBlocked(manager, gateIndex);
}

bool UPlatformIntegration::gateMotionIsBlocked(uint8 gateIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::gateMotionIsBlocked(manager, gateIndex);
}

void UPlatformIntegration::gateSetBlock(uint8 gateIndex, bool blocked)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::gateSetBlock(manager, gateIndex, blocked);
}

bool UPlatformIntegration::boxButtonPressed(uint8 boxIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::boxButtonPressed(manager, boxIndex);
}

bool UPlatformIntegration::boxIsBlocked(uint8 boxIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::boxIsBlocked(manager, boxIndex);
}

bool UPlatformIntegration::boxMotionIsBlocked(uint8 boxIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::boxMotionIsBlocked(manager, boxIndex);
}

void UPlatformIntegration::boxSetBlock(uint8 boxIndex, bool blocked)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::boxSetBlock(manager, boxIndex, blocked);
}

bool UPlatformIntegration::perimeterIsFree()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::perimeterIsFree(manager);
}

int UPlatformIntegration::effectGetAnalog(uint8 effectBoardIndex, int effectNumber)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::effectGetAnalog(manager, effectBoardIndex, effectNumber);
}

void UPlatformIntegration::effectSetAnalog(uint8 effectBoardIndex, int effectNumber, int value)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::effectSetAnalog(manager, effectBoardIndex, effectNumber, value);
}

bool UPlatformIntegration::effectGetDiscrete(uint8 effectBoardIndex, int effectNumber)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::effectGetDiscrete(manager, effectBoardIndex, effectNumber);
}

void UPlatformIntegration::effectSetDiscrete(uint8 effectBoardIndex, int effectNumber, bool value)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::effectSetDiscrete(manager, effectBoardIndex, effectNumber, value);
}

int UPlatformIntegration::effectGetSpecial(uint8 effectBoardIndex, int effectNumber)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::effectGetSpecial(manager, effectBoardIndex, effectNumber);
}

void UPlatformIntegration::effectSetSpecial(uint8 effectBoardIndex, int effectNumber, int value)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::effectSetSpecial(manager, effectBoardIndex, effectNumber, value);
}

void UPlatformIntegration::effectPressPcButton()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::effectPressPcButton(manager);
}

void UPlatformIntegration::effectReleasePcButton()
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::effectReleasePcButton(manager);
}

bool UPlatformIntegration::supervisorGetInput(uint8 supervisorIndex, int inputIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::supervisorGetInput(manager, supervisorIndex, inputIndex);
}

bool UPlatformIntegration::supervisorGetOutput(uint8 supervisorIndex, int outputIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	return YottoEffects::supervisorGetOutput(manager, supervisorIndex, outputIndex);
}

void UPlatformIntegration::supervisorSetOutput(uint8 supervisorIndex, int outputIndex, bool value)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	YottoEffects::supervisorSetOutput(manager, supervisorIndex, outputIndex, value);
}

void UPlatformIntegration::driveSetCoord(uint8 driveIndex, float coord)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}

	if (config.bInvertDriveAxis)
		YottoEffects::driveSetCoord(manager, 1 - driveIndex, coord);
	else
		YottoEffects::driveSetCoord(manager, driveIndex, coord);
}

float UPlatformIntegration::driveGetCoord(uint8 driveIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}

	if (config.bInvertDriveAxis)
		return YottoEffects::driveGetCoord(manager, 1 - driveIndex);
	else
		return YottoEffects::driveGetCoord(manager, driveIndex);
}

FString UPlatformIntegration::driveGetStatus(uint8 driveIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}

	if (config.bInvertDriveAxis)
		return FString(UTF8_TO_TCHAR(YottoEffects::driveGetStatus(manager, 1 - driveIndex)));
	else
		return FString(UTF8_TO_TCHAR(YottoEffects::driveGetStatus(manager, driveIndex)));	
}

FString UPlatformIntegration::driveGetResetStatus(uint8 driveIndex)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}
	
	if (config.bInvertDriveAxis)
		return FString(UTF8_TO_TCHAR(YottoEffects::driveGetResetStatus(manager, 1 - driveIndex)));
	else
		return FString(UTF8_TO_TCHAR(YottoEffects::driveGetResetStatus(manager, driveIndex)));
}

void UPlatformIntegration::driveSendCommand(uint8 driveIndex, uint8 commandCode)
{
	if (manager == 0)
	{
		manager = YottoEffects::CreatePlatformManager("127.0.0.1", 5555);
	}

	if (config.bInvertDriveAxis)
		YottoEffects::driveSendCommand(manager, 1 - driveIndex, commandCode);
	else
		YottoEffects::driveSendCommand(manager, driveIndex, commandCode);
}

int UPlatformIntegration::getDLLVersion()
{
	auto version = YottoEffects::getVersion();
	return version;
}