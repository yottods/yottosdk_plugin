// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "YottoSDK.h"
#include "Core.h"
#include "ModuleManager.h"
#include "IPluginManager.h"
#include "../Yotto/PlatformManager/Includes/PlatformManagerFactory.h"

void FYottoSDKModule::StartupModule()
{

}

void FYottoSDKModule::ShutdownModule()
{

}
	
IMPLEMENT_MODULE(FYottoSDKModule, YottoSDK)