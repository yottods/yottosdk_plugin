// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "YottoEffectData.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EYottoEffectType : uint8
{
    VE_Wind 	    UMETA(DisplayName = "Wind"),
    VE_WindSecond   UMETA(DisplayName = "WindSecond"),
    VE_Water 	    UMETA(DisplayName = "Water"),
    VE_Kick 	    UMETA(DisplayName = "Kick"),
    VE_WarmWind 	UMETA(DisplayName = "WarmWind"),
    VE_Vibration 	UMETA(DisplayName = "Vibration"),
    VE_Light 	    UMETA(DisplayName = "Light")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EYottoEffectPosition : uint8
{
    VE_ForwardLeft 	    UMETA(DisplayName = "ForwardLeft"),
    VE_ForwardRight 	UMETA(DisplayName = "ForwardRight"),
    VE_BackwardsLeft 	UMETA(DisplayName = "BackwardsLeft"),
    VE_BackwardsRight 	UMETA(DisplayName = "BackwardsRight"),
    VE_All 	            UMETA(DisplayName = "All"),
    VE_None 	        UMETA(DisplayName = "None"),
    VE_Default       	UMETA(DisplayName = "Default")
};

USTRUCT(BlueprintType)
struct FSingleEffect
{
    GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite)
        EYottoEffectType effectType = EYottoEffectType::VE_Wind;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        bool isAnalog = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        bool isDiscrete = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        bool isSpecial = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        bool isDefault = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        int32 effectNumber = -1;
};

USTRUCT(BlueprintType)
struct FEffectPositionContainer
{
    GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite)
        EYottoEffectPosition effectsPosition = EYottoEffectPosition::VE_ForwardLeft;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        TArray<FSingleEffect> effects;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        int32 boardNumber = -1;
};

USTRUCT(BlueprintType)
struct FConfigData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bUseEffects = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bSimulateCoordinates = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bInvertDriveAxis = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsKidMode = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FEffectPositionContainer> effectBoards;
};

USTRUCT(BlueprintType)
struct FEffectTuple
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DurationLeft;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 BoardIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 EffectNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Value;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsAnalog;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsDiscrete;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsSpecial;
};

static class EnumHelper
{
public:
	template<typename T>
	static FString EnumToString(const FString& enumName, const T value, const FString& defaultValue)
	{
		UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, *enumName, true);
		return pEnum
			? ExpandEnumString(pEnum->GetNameByIndex(static_cast<uint8>(value)).ToString(), enumName)
			: defaultValue;
	}

	static FString ExpandEnumString(const FString& name, const FString& enumName)
	{
		FString expanded(name);
		FString spaceLetter("");
		FString spaceNumber("");
		FString search("");
		expanded.ReplaceInline(*enumName, TEXT(""), ESearchCase::CaseSensitive);
		expanded.ReplaceInline(TEXT("::"), TEXT(""), ESearchCase::CaseSensitive);
		for (TCHAR letter = 'A'; letter <= 'Z'; ++letter)
		{
			search = FString::Printf(TEXT("%c"), letter);
			spaceLetter = FString::Printf(TEXT(" %c"), letter);
			expanded.ReplaceInline(*search, *spaceLetter, ESearchCase::CaseSensitive);
		}
		for (TCHAR number = '0'; number <= '9'; ++number)
		{
			search = FString::Printf(TEXT("%c"), number);
			spaceNumber = FString::Printf(TEXT(" %c"), number);
			expanded.ReplaceInline(*search, *spaceNumber, ESearchCase::CaseSensitive);
		}
		expanded.ReplaceInline(TEXT("_"), TEXT(" -"), ESearchCase::CaseSensitive);
		expanded = expanded.RightChop(1).Trim().TrimTrailing();
		return expanded;
	}
};