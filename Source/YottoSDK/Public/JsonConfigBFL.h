// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "Json.h"
#include "JsonUtilities.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "JsonConfigBFL.generated.h"

UCLASS()
class YOTTOSDK_API UJsonConfigBFL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Config")
	static bool SaveConfigFile(FString fileName, FString string);

	UFUNCTION(BlueprintCallable, Category = "Config")
	static bool SaveFile(FString directory, FString fileName, FString string);

	UFUNCTION(BlueprintCallable, Category = "Config")
	static bool LoadConfigFile(FString fileName, FString &result);

	UFUNCTION(BlueprintCallable, Category = "Config")
	static bool LoadFile(FString directory, FString fileName, FString &result);

	// https://forums.unrealengine.com/showthread.php?56537-Tutorial-How-to-accept-wildcard-structs-in-your-UFUNCTIONs&p=206131#post206131
	// https://github.com/EverNewJoy/VictoryPlugin/blob/master/Source/VictoryBPLibrary/Public/VictoryBPFunctionLibrary.h
	// sources from JsonObjectConverter::UStructToJsonObject
	UFUNCTION(BlueprintCallable, Category = "Json", CustomThunk, meta = (CustomStructureParam = "AnyStruct"))
	static FString SerializeStructByJson(UProperty* AnyStruct);

	DECLARE_FUNCTION(execSerializeStructByJson)
	{
		// Steps into the stack, walking to the next property in it
		Stack.Step(Stack.Object, NULL);

		// Grab the last property found when we walked the stack
		// This does not contains the property value, only its type information
		UStructProperty* StructProperty = ExactCast<UStructProperty>(Stack.MostRecentProperty);

		// Grab the base address where the struct actually stores its data
		// This is where the property value is truly stored
		void* StructPtr = Stack.MostRecentPropertyAddress;

		// We need this to wrap up the stack
		P_FINISH;

		// Return value
		*(FString*)RESULT_PARAM = SerializeStructByProperty(StructProperty, StructPtr);
	}

	// https://forums.unrealengine.com/showthread.php?56537-Tutorial-How-to-accept-wildcard-structs-in-your-UFUNCTIONs&p=206131#post206131
	// https://github.com/EverNewJoy/VictoryPlugin/blob/master/Source/VictoryBPLibrary/Public/VictoryBPFunctionLibrary.h
	// sources from JsonObjectConverter::UStructToJsonObject
	UFUNCTION(BlueprintCallable, Category = "Json", CustomThunk, meta = (CustomStructureParam = "AnyStruct"))
	static void DeserializeStructByJson(UProperty* AnyStruct, const FString& jsonString);

	DECLARE_FUNCTION(execDeserializeStructByJson)
	{
		// Steps into the stack, walking to the next property in it
		//Stack.Step(Stack.Object, NULL);

		//// Grab the last property found when we walked the stack
		//// This does not contains the property value, only its type information
		//UStructProperty* StructProperty = ExactCast<UStructProperty>(Stack.MostRecentProperty);

		// Grab the base address where the struct actually stores its data
		// This is where the property value is truly stored
		//void* StructPtr = Stack.MostRecentPropertyAddress;

		Stack.StepCompiledIn<UStructProperty>(NULL);
		void* structPtr = Stack.MostRecentPropertyAddress;
		auto structProperty = Cast<UStructProperty>(Stack.MostRecentProperty);

		Stack.StepCompiledIn<UStrProperty>(NULL);
		void* strPtr = Stack.MostRecentPropertyAddress;
		auto strProperty = Cast<UStrProperty>(Stack.MostRecentProperty);

		// We need this to wrap up the stack
		P_FINISH;

		DeserializeStructPropertyByJson(structProperty, structPtr, strProperty->GetPropertyValue(strPtr));
	}
	
	// https://forums.unrealengine.com/showthread.php?56537-Tutorial-How-to-accept-wildcard-structs-in-your-UFUNCTIONs&p=206131#post206131
	// https://github.com/EverNewJoy/VictoryPlugin/blob/master/Source/VictoryBPLibrary/Public/VictoryBPFunctionLibrary.h
	// sources from JsonObjectConverter::UStructToJsonObject
	static FORCEINLINE FString SerializeStructByProperty(UStructProperty* StructProperty, void* StructPtr)
	{
		TSharedRef<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
		if (!FJsonObjectConverter::UStructToJsonObject(StructProperty->Struct, StructPtr, JsonObject, 0, 0, nullptr))
			return TEXT("Invalid");

		FString jsonString;
		TSharedRef< TJsonWriter<> > writer = TJsonWriterFactory<>::Create(&jsonString);
		if (!FJsonSerializer::Serialize(JsonObject, writer))
			return TEXT("Invalid");

		return jsonString;
	}

	// https://forums.unrealengine.com/showthread.php?56537-Tutorial-How-to-accept-wildcard-structs-in-your-UFUNCTIONs&p=206131#post206131
	// https://github.com/EverNewJoy/VictoryPlugin/blob/master/Source/VictoryBPLibrary/Public/VictoryBPFunctionLibrary.h
	// sources from JsonObjectConverter::JsonObjectToUStruct
	static FORCEINLINE void DeserializeStructPropertyByJson(UStructProperty* StructProperty, void* StructPtr, FString jsonString)
	{
		TSharedPtr<FJsonObject> jsonPtr;
		TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(jsonString);
		if (!FJsonSerializer::Deserialize(reader, jsonPtr))
		{
			UE_LOG(LogScript, Error, TEXT("Cannot deserialize string"));
			return;
		}


		if (!FJsonObjectConverter::JsonObjectToUStruct(jsonPtr.ToSharedRef(), StructProperty->Struct, StructPtr, 0, 0))
		{
			UE_LOG(LogScript, Error, TEXT("Cannot convert json object to struct"));
			return;
		}
	}
};
