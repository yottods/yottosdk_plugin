// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EngineUtils.h"
#include "GameFramework/Actor.h"
//#include "Json.h"
//#include "JsonUtilities.h"
#include "Public/YottoEffectData.h"
#include "Public/JsonConfigBFL.h"
#include "Public/PlatformIntegration.h"
#include "YottoEffectController.generated.h"

UCLASS()
class YOTTOSDK_API AYottoEffectController : public AActor
{
	GENERATED_BODY()
	
public:	
	AYottoEffectController();

protected:

	UPROPERTY()
	FConfigData ConfigData;

	UPROPERTY()
	bool bIsStarted = false;
	
	UPROPERTY()
	TArray<FEffectTuple> ActiveEffects;

	UPROPERTY()
	FString ConfigFileName = "YottoConfig";

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void PrintLog(FString message, uint8 boardIndex, int32 effectNumber, int32 value, bool isAnalog, bool isDiscrete, bool isSpecial);

	void DisableEffect(int32 index);

	void GenerateNewConfigFile();

	int32 GetActiveIndex(uint8 boardIndex, int32 effectNumber, bool isAnalog, bool isDiscrete, bool isSpecial);

	void AddActiveEffect(float duration, uint8 boardIndex, int32 effectNumber, uint32 value, bool isAnalog, bool isDiscrete, bool isSpecial);

	bool GetEffectStruct(EYottoEffectType effectType, EYottoEffectPosition effectPosition, FEffectPositionContainer &OutPositionContainer, FSingleEffect &OutEffectData);

	void DriveSimulation();

	void ApplyEffectInternal(EYottoEffectType effectType, EYottoEffectPosition effectPosition, float duration, uint32 value);

public:	

	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable, Category = "YottoSDK|EffectController")
	bool IsConnected();

	UFUNCTION(BlueprintCallable, Category = "YottoSDK|EffectController", meta = (WorldContext = "WorldContextObject"))
	static void ApplyEffect(UObject* WorldContextObject, EYottoEffectType effectType, EYottoEffectPosition effectPosition, float duration, int32 value);
	

};
