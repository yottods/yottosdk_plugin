// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "YottoSDK.h"
#include "Engine.h"
#include "../Yotto/PlatformManager/Includes/PlatformManagerFactory.h"
//#include "PlatformManagerFactory.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "YottoEffectData.h"
#include "PlatformIntegration.generated.h"


/**
 * 
 */
UCLASS()
class YOTTOSDK_API UPlatformIntegration : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
		
private:	

	

public:

	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void SetConfigData(const FConfigData& newConfigData);

	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool isGame();
	UFUNCTION(BlueprintPure, Category = "YottoSDK", meta = (CompactNodeTitle = "Is Connected"))
	static bool isConnected();
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void setUpdateDelay(uint8 delay);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static int getUpdateDelay();
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void setEnabled(bool enabled);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void setDebug(bool debugEnabled);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void restart();
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void startSession(bool isKidMode);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void stopSession();

	// Joysticks
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 joysticksGetCount();
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool joystickGetButton(uint8 joystickIndex, int buttonIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static float joystickGetX(uint8 joystickIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static float joystickGetY(uint8 joystickIndex);

	// Arcs
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 arcsGetCount() ;
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool arcIsClosed(uint8 arcIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool arcIsPreopened(uint8 arcIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool arcIsBroken(uint8 arcIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool arcIsBlocked(uint8 arcIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool arcMotionIsBlocked(uint8 arcIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void arcSetBlock(uint8 arcIndex, bool blocked);

	// Belts
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 beltsGetCount() ;
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool beltIsClosed(uint8 beltIndex);

	// Gates
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 gatesGetCount() ;
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool gateIsClosed(uint8 gateIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool gateIsBlocked(uint8 gateIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool gateMotionIsBlocked(uint8 gateIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void gateSetBlock(uint8 gateIndex, bool blocked);

	// Boxes
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 boxesGetCount();
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool boxButtonPressed(uint8 boxIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool boxIsBlocked(uint8 boxIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool boxMotionIsBlocked(uint8 boxIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void boxSetBlock(uint8 boxIndex, bool blocked);

	// Perimeter
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool perimeterIsFree();

	// Effects
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 effectsGetCount() ;
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static int effectGetAnalog(uint8 effectBoardIndex, int effectNumber);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void effectSetAnalog(uint8 effectBoardIndex, int effectNumber, int value);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool effectGetDiscrete(uint8 effectBoardIndex, int effectNumber);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void effectSetDiscrete(uint8 effectBoardIndex, int effectNumber, bool value);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static int effectGetSpecial(uint8 effectBoardIndex, int effectNumber);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void effectSetSpecial(uint8 effectBoardIndex, int effectNumber, int value);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void effectPressPcButton();
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void effectReleasePcButton();

	// Supervisors
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 supervisorsGetCount() ;
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool supervisorGetInput(uint8 supervisorIndex, int inputIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static bool supervisorGetOutput(uint8 supervisorIndex, int outputIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void supervisorSetOutput(uint8 supervisorIndex, int outputIndex, bool value);

	// ServoDrives
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static uint8 drivesGetCount() ;
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void driveSetCoord(uint8 driveIndex, float coord);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static float driveGetCoord(uint8 driveIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static FString driveGetStatus(uint8 driveIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static FString driveGetResetStatus(uint8 driveIndex);
	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static void driveSendCommand(uint8 driveIndex, uint8 commandCode);

	UFUNCTION(BlueprintCallable, Category = "YottoSDK")
	static int getDLLVersion();
};
