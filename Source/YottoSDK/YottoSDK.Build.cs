// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System;
using System.IO;

public class YottoSDK : ModuleRules
{
    private string YottoPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../Yotto/")); }
    }

    public YottoSDK(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				"YottoSDK/Public",
                "../Yotto/PlatformManager/Includes"
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"YottoSDK/Private",
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"Projects"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
                "CoreUObject",
                "Engine",
                "Json",
                "JsonUtilities",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

        LoadZeroMQ(Target);

        LoadPlatformManager(Target);
    }

    public bool LoadPlatformManager(ReadOnlyTargetRules Target)
    {
        bool isLibrarySupported = false;

        if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
        {
            isLibrarySupported = true;

            string PlatformString = (Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86";
            string LibrariesPath = Path.Combine(YottoPath, "PlatformManager", "Libraries");

            /*
            test your path with:
            using System; // Console.WriteLine("");/**/
            Console.WriteLine("... LibrariesPath -> " + Path.Combine(LibrariesPath, "PlatformManager." + PlatformString + ".lib"));
            /**/

            PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "PlatformManager." + PlatformString + ".lib"));
        }

        Console.WriteLine("... isLibrarySupported -> " + isLibrarySupported);

        if (isLibrarySupported)
        {
            // Include path
            PublicIncludePaths.Add(Path.Combine(YottoPath, "PlatformManager", "Includes"));
            Console.WriteLine("... PublicIncludePaths -> " + Path.Combine(YottoPath, "PlatformManager", "Includes"));
        }

        Definitions.Add(string.Format("WITH_PLATFORM_MANAGER_BINDING={0}", isLibrarySupported ? 1 : 0));

        return isLibrarySupported;
    }

    public bool LoadZeroMQ(ReadOnlyTargetRules Target)
    {
        bool isLibrarySupported = false;

        if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
        {
            isLibrarySupported = true;

            string LibrariesPath = Path.Combine(YottoPath, "ZeroMQ", "Libraries");

            Console.WriteLine("... LibrariesPath -> " + Path.Combine(LibrariesPath, "libzmq.lib"));

            PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "libzmq.lib"));
        }

        Console.WriteLine("... isLibrarySupported -> " + isLibrarySupported);

        if (isLibrarySupported)
        {
            PublicIncludePaths.Add(Path.Combine(YottoPath, "ZeroMQ", "Includes"));
            Console.WriteLine("... PublicIncludePaths -> " + Path.Combine(YottoPath, "ZeroMQ", "Includes"));
        }

        Definitions.Add(string.Format("WITH_ZERO_MQ_BINDING={0}", isLibrarySupported ? 1 : 0));

        return isLibrarySupported;
    }
}
