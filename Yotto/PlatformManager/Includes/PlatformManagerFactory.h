#ifndef _PLATFORM_MANAGER_FABRIC
#define _PLATFORM_MANAGER_FABRIC

#include <string>
#include "IPlatformManager.h"
#include "export.h"

class EXPORTIT PlatformManagerFactory
{
public:
	static IPlatformManager* Create(std::string ip, int port);
};

extern "C"
{
	IPlatformManager* Create2(std::string ip, int port);
	float Sum(float a, float b);
}

//typedef void* PlatformManagerPTR;

namespace YottoEffects
{
	extern "C" unsigned int getVersion();

	extern "C" void* CreatePlatformManager(const char* ip, int port);

	extern "C" void DisposePlatformManager(void* manager);

	extern "C" bool isGame(void* manager);

	extern "C" bool isLicenseOk(void* manager);

	extern "C" bool isConnected(void* manager);

	extern "C" void setUpdateDelay(void* manager, unsigned int delay);

	extern "C" int getUpdateDelay(void* manager);

	extern "C" void setEnabled(void* manager, bool enabled);

	extern "C" void setDebug(void* manager, bool debugEnabled);

	extern "C" void restart(void* manager);

	extern "C" void startSession(void* manager, bool isKidMode);

	extern "C" void stopSession(void* manager);

	// Joysticks
	extern "C" unsigned int joysticksGetCount(void* manager);

	extern "C" bool joystickGetButton(void* manager, unsigned int joystickIndex, int buttonIndex);

	extern "C" float joystickGetX(void* manager, unsigned int joystickIndex);

	extern "C" float joystickGetY(void* manager, unsigned int joystickIndex);

	extern "C" void joystickCalibrate(void* manager, unsigned int joystickIndex, JoystickCalibratePosition position);

	// Arcs
	extern "C" unsigned int arcsGetCount(void* manager);

	extern "C" bool arcIsPreopened(void* manager, unsigned int arcIndex);

	extern "C" bool arcIsOpened(void* manager, unsigned int arcIndex);

	extern "C" bool arcIsClosed(void* manager, unsigned int arcIndex);

	extern "C" bool arcIsBlocked(void* manager, unsigned int arcIndex);

	extern "C" bool arcMotionIsBlocked(void* manager, unsigned int arcIndex);

	extern "C" void arcSetBlock(void* manager, unsigned int arcIndex, bool blocked);

	extern "C" bool arcIsBroken(void* manager, unsigned int arcIndex);

	// Belts
	extern "C" unsigned int beltsGetCount(void* manager);

	extern "C" bool beltIsClosed(void* manager, unsigned int beltIndex);

	// Gates
	extern "C" unsigned int gatesGetCount(void* manager);

	extern "C" bool gateIsClosed(void* manager, unsigned int gateIndex);

	extern "C" bool gateIsBlocked(void* manager, unsigned int gateIndex);

	extern "C" bool gateMotionIsBlocked(void* manager, unsigned int gateIndex);

	extern "C" void gateSetBlock(void* manager, unsigned int gateIndex, bool blocked);

	// Boxes
	extern "C" unsigned int boxesGetCount(void* manager);

	extern "C" bool boxButtonPressed(void* manager, unsigned int boxIndex);

	extern "C" bool boxIsBlocked(void* manager, unsigned int boxIndex);

	extern "C" bool boxMotionIsBlocked(void* manager, unsigned int boxIndex);

	extern "C" void boxSetBlock(void* manager, unsigned int boxIndex, bool blocked);

	// Perimeter
	extern "C" bool perimeterIsFree(void* manager);

	// Effects
	extern "C" unsigned int effectsGetCount(void* manager);

	extern "C" int effectGetAnalog(void* manager, unsigned int effectBoardIndex, int effectNumber);

	extern "C" void effectSetAnalog(void* manager, unsigned int effectBoardIndex, int effectNumber, int value);

	extern "C" bool effectGetDiscrete(void* manager, unsigned int effectBoardIndex, int effectNumber);

	extern "C" void effectSetDiscrete(void* manager, unsigned int effectBoardIndex, int effectNumber, bool value);

	extern "C" int effectGetSpecial(void* manager, unsigned int effectBoardIndex, int effectNumber);

	extern "C" void effectSetSpecial(void* manager, unsigned int effectBoardIndex, int effectNumber, int value);

	extern "C" void effectPressPcButton(void* manager);

	extern "C" void effectReleasePcButton(void* manager);

	extern "C" void effectTvButton(void* manager, TvButton button);

	// Supervisors
	extern "C" unsigned int supervisorsGetCount(void* manager);

	extern "C" bool supervisorGetInput(void* manager, unsigned int supervisorIndex, int inputIndex);

	extern "C" bool supervisorGetOutput(void* manager, unsigned int supervisorIndex, int outputIndex);

	extern "C" void supervisorSetOutput(void* manager, unsigned int supervisorIndex, int outputIndex, bool value);

	// ServoDrives
	extern "C" unsigned int drivesGetCount(void* manager);

	extern "C" void driveSetCoord(void* manager, unsigned int driveIndex, float coord);

	extern "C" float driveGetSentCoord(void* manager, unsigned int driveIndex);

	extern "C" float driveGetCoord(void* manager, unsigned int driveIndex);

	extern "C" char driveGetStatus(void* manager, unsigned int driveIndex);

	extern "C" char driveGetResetStatus(void* manager, unsigned int driveIndex);

	extern "C" void driveSendCommand(void* manager, unsigned int driveIndex, unsigned int commandCode);
}

#endif
