#ifndef _STATE_ENUMS
#define _STATE_ENUMS

enum GameMode {
	ADULT,
	KID
};

enum TvButton
{
	POWER = 1,
	RIGHT = 2,
	OK = 3,
	MODE_3D = 4
};


enum JoystickCalibratePosition {
	SET_X_RIGHT = 1,
	SET_X_LEFT = 2,
	SET_Y_FRONT = 3,
	SET_Y_BACK = 4,
	SET_CENTER = 5
};


#endif // _STATE_ENUMS