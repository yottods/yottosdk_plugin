#ifndef _PLATFORM_MANAGER_INTERFACE_
#define _PLATFORM_MANAGER_INTERFACE_

#include "StateEums.h"
#include "export.h"

class EXPORTIT IPlatformManager
{
public:
	virtual ~IPlatformManager()
	{
	}

	virtual bool isGame() = 0;
	virtual bool isConnected() = 0;
	virtual void setUpdateDelay(unsigned int delay) = 0;
	virtual int getUpdateDelay() = 0;
	virtual void setEnabled(bool enabled) = 0;
	virtual void setDebug(bool debugEnabled) = 0;

	virtual void restart() = 0;

	virtual void startSession(bool isKidMode) = 0;
	virtual void stopSession() = 0;

	// Joysticks
	virtual unsigned int joysticksGetCount() = 0;
	virtual bool joystickGetButton(unsigned int joystickIndex, int buttonIndex) = 0;
	virtual float joystickGetX(unsigned int joystickIndex) = 0;
	virtual float joystickGetY(unsigned int joystickIndex) = 0;
	virtual void joystickCalibrate(unsigned int joystickIndex, JoystickCalibratePosition position) = 0;

	// Arcs
	virtual unsigned int arcsGetCount() = 0;
	virtual bool arcIsOpened(unsigned int arcIndex) = 0;
	virtual bool arcIsClosed(unsigned int arcIndex) = 0;
	virtual bool arcIsPreopened(unsigned int arcIndex) = 0;
	virtual bool arcIsBroken(unsigned int arcIndex) = 0;
	virtual bool arcIsBlocked(unsigned int arcIndex) = 0;
	virtual bool arcMotionIsBlocked(unsigned int arcIndex) = 0;
	virtual void arcSetBlock(unsigned int arcIndex, bool blocked) = 0;

	// Belts
	virtual unsigned int beltsGetCount() = 0;
	virtual bool beltIsClosed(unsigned int beltIndex) = 0;

	// Gates
	virtual unsigned int gatesGetCount() = 0;
	virtual bool gateIsClosed(unsigned int gateIndex) = 0;
	virtual bool gateIsBlocked(unsigned int gateIndex) = 0;
	virtual bool gateMotionIsBlocked(unsigned int gateIndex) = 0;
	virtual void gateSetBlock(unsigned int gateIndex, bool blocked) = 0;

	// Boxes
	virtual unsigned int boxesGetCount() = 0;
	virtual bool boxButtonPressed(unsigned int boxIndex) = 0;
	virtual bool boxIsBlocked(unsigned int boxIndex) = 0;
	virtual bool boxMotionIsBlocked(unsigned int boxIndex) = 0;
	virtual void boxSetBlock(unsigned int boxIndex, bool blocked) = 0;

	// Perimeter
	virtual bool perimeterIsFree() = 0;

	// Effects
	virtual unsigned int effectsGetCount() = 0;
	virtual int effectGetAnalog(unsigned int effectBoardIndex, int effectNumber) = 0;
	virtual void effectSetAnalog(unsigned int effectBoardIndex, int effectNumber, int value) = 0;
	virtual bool effectGetDiscrete(unsigned int effectBoardIndex, int effectNumber) = 0;
	virtual void effectSetDiscrete(unsigned int effectBoardIndex, int effectNumber, bool value) = 0;
	virtual int effectGetSpecial(unsigned int effectBoardIndex, int effectNumber) = 0;
	virtual void effectSetSpecial(unsigned int effectBoardIndex, int effectNumber, int value) = 0;
	virtual void effectPressPcButton() = 0;
	virtual void effectReleasePcButton() = 0;
	virtual void effectPressTvButton(TvButton button) = 0;

	// Supervisors
	virtual unsigned int supervisorsGetCount() = 0;
	virtual bool supervisorGetInput(unsigned int supervisorIndex, int inputIndex) = 0;
	virtual bool supervisorGetOutput(unsigned int supervisorIndex, int outputIndex) = 0;
	virtual void supervisorSetOutput(unsigned int supervisorIndex, int outputIndex, bool value) = 0;

	// ServoDrives
	virtual unsigned int drivesGetCount() = 0;
	virtual void driveSetCoord(unsigned int driveIndex, float coord) = 0;
	virtual float driveGetSentCoord(unsigned int driveIndex) = 0;
	virtual float driveGetCoord(unsigned int driveIndex) = 0;
	virtual char driveGetStatus(unsigned int driveIndex) = 0;
	virtual char driveGetResetStatus(unsigned int driveIndex) = 0;
	virtual void driveSendCommand(unsigned int driveIndex, unsigned int commandCode) = 0;

	virtual float sum(float a, float b) = 0;
	virtual int sumint(int a, int b) = 0;
};

#endif